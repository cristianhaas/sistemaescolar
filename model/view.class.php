<?php 
class View{
	private $tipo_usuario;
	// $pagina vem do arquivo config
	// $categoria vem do arquivo config
	public $view=0;
	public $edit=0;
	public $insert=0;
	private $pag;
	private $cat;
	private $conexao;
	private $id_escola;
	function __construct($conexao,$pagina,$categoria,$tipo_usuario,$id_escola){
	$this->conexao=$conexao;
    $this->pag=$pagina;
 	$this->tipo_usuario=$tipo_usuario;
  	$this->cat=$categoria;
  	$this->id_escola=$id_escola;
	}
	public function show(){
		// captura os dados da pagina
		$dados=$this->buscaDadosPagina();
		// se popssuir permissao, exibe a pagina
		if($dados==1)
			return $this->exibeView("");
		// caso a pagina não exista no mapeamento
        else if($dados==-1)
        	return $this->exibeView("erro-404");
        // caso esteja no indexz
        else if($dados=="index")
        	return $this->exibeView("index");	
        // caso a pessoa não possua acesso a pagina
        else
        	return $this->exibeView("erro-permissao");	
		
	}
	// buscar no banco os dados de permissão da pagina a ser visualizada
	private function buscaDadosPagina(){
		// se não pertencer a ctegoria view
		if($this->cat=="visualizar"){
			$this->view=1;
		} else if($this->cat=="editar"){
			$this->edit=1;
		} else if($this->cat=="incluir"){
			$this->insert=1;
		}
		else{
			return "index";	
		}
		//buscar a pagina nas permissões (todas as paginas devem estar mapeadas)
		$preparaBusca=$this->conexao->prepare("SELECT * FROM chf_tabela_permissoes tp, chf_menu m WHERE m.id_menu=tp.id_menu AND m.nome_link=? AND id_escola=?");
		$e=$preparaBusca->execute(array($this->pag,$this->id_escola));
		// verifica se não encontrou a pagina e retorna erro para mostra um 404
		if($preparaBusca->rowCount()==false)
			return -1;

		// continua a verificação se o usuariio pode acessar aquela pagina
		$preparaBusca=$this->conexao->prepare("SELECT * FROM chf_tabela_permissoes tp, chf_menu m WHERE  m.id_menu=tp.id_menu AND m.nome_link=? AND tp.tipo_usuario=? AND ((tp.visualizar=? AND tp.visualizar=1) OR (tp.editar=? AND tp.editar=1) OR(tp.incluir=? AND tp.incluir=1) AND id_escola=?)");
		$e=$preparaBusca->execute(array($this->pag,$this->tipo_usuario,$this->view,$this->edit,$this->insert,$this->id_escola));
		// se não possuir permisao, retorna falso
		if($preparaBusca->rowCount()==false)
		return false;
	    // e se possuir retorna os dados da pagina 
        else
		return 1;
	}
    public function getView(){
    	$preparaBusca=$this->conexao->prepare("SELECT tp.visualizar FROM chf_tabela_permissoes tp, chf_menu m WHERE  m.id_menu=tp.id_menu AND m.nome_link=? AND tp.tipo_usuario=? AND id_escola=?");
		$preparaBusca->execute(array($this->pag,$this->tipo_usuario,$this->id_escola));
        $resultado=$preparaBusca->fetch();
		// se não possuir permisao, retorna falso
		if($preparaBusca->rowCount()==false)
		return false;
		else
		return $resultado['visualizar'];
    }
    public function getEdit(){
    	$preparaBusca=$this->conexao->prepare("SELECT tp.editar FROM chf_tabela_permissoes tp, chf_menu m WHERE  m.id_menu=tp.id_menu AND m.nome_link=? AND tp.tipo_usuario=? AND id_escola=?");
		$preparaBusca->execute(array($this->pag,$this->tipo_usuario,$this->id_escola));
        $resultado=$preparaBusca->fetch();
		// se não possuir permisao, retorna falso
		if($preparaBusca->rowCount()==false)
		return false;
		else
		return $resultado['editar'];
    }
     public function getInsert(){
    	$preparaBusca=$this->conexao->prepare("SELECT tp.incluir FROM chf_tabela_permissoes tp, chf_menu m WHERE  m.id_menu=tp.id_menu AND m.nome_link=? AND tp.tipo_usuario=? AND id_escola=?");
		$preparaBusca->execute(array($this->pag,$this->tipo_usuario,$this->id_escola));
        $resultado=$preparaBusca->fetch();
		// se não possuir permisao, retorna falso
		if($preparaBusca->rowCount()==false)
		return false;
		else
		return $resultado['incluir'];
    }
    public function getDelete(){
    	$preparaBusca=$this->conexao->prepare("SELECT tp.excluir FROM chf_tabela_permissoes tp, chf_menu m WHERE  m.id_menu=tp.id_menu AND m.nome_link=? AND tp.tipo_usuario=? AND id_escola=?");
		$preparaBusca->execute(array($this->pag,$this->tipo_usuario,$this->id_escola));
        $resultado=$preparaBusca->fetch();
		// se não possuir permisao, retorna falso
		if($preparaBusca->rowCount()==false)
		return false;
		else
		return $resultado['excluir'];
    }
    private function exibeView($pagina){
    if($pagina=="")
    return("view/".$this->cat."-".$this->pag.".php");
	else
	return("view/$pagina.php");
    }

}

?>