<?php
class Modal {
	private $texto;
	private $titulo_texto;
	private $titulo;
	private $texto_botao_confirmar;
    private $acao;
	function setTexto( $texto ) {
		$this->texto = $texto;
	}

	function setTitulo( $titulo ) {
		$this->titulo = $titulo;
	}
	function setAcao( $acao ) {
		$this->acao = $acao;
	}

	function setTituloTexto( $titulo_texto ) {
		$this->titulo_texto = $titulo_texto;
	}

	function setTextoBotaoConfirmar( $texto_botao_confirmar ) {
		$this->texto_botao_confirmar = $texto_botao_confirmar;
	}

	function show() {

		echo "<div class='modal fade bs-example-modal-lg' tabindex='-1' role='dialog' aria-hidden='false' data-show='true' id='modal'>";
		echo "	<div class='modal-dialog modal-lg'>";
		echo "		<div class='modal-content'>";
		echo "			<div class='modal-header'>";
		echo "				<button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>×</span></button>";
		echo "				<h4 class='modal-title' id='myModalLabel'>$this->titulo</h4>";
		echo "			</div>";
		echo "			<div class='modal-body'>";
		echo "				<h4>$this->titulo_texto</h4>";
		echo $this->texto;
		echo "			</div>";
		echo "			<div class='modal-footer'>";
		echo "				<button type='button' class='btn btn-default' data-dismiss='modal'>Fechar</button>";
		echo "				<button type='button' class='btn btn-primary' onclick='$this->acao'>$this->texto_botao_confirmar</button>";
		echo "			</div>";
		echo "		</div>";
		echo "	</div>";
		echo "</div>";
	}
}
?>
