				<?php
				$botao = isset( $_POST[ 'cadastrar' ] ) ? $_POST[ 'cadastrar' ] : '';

				if ( $botao == "Cadastrar" ) {

					$consultor = new Consultor();
					$consultor->setNome( $_POST[ 'nome' ] );
					$consultor->setPais_id( $_POST[ 'pais' ] );
					$consultor->setEstado_id( $_POST[ 'estado' ] );
					$consultor->setCidade_id( $_POST[ 'cidade' ] );
					$consultor->setTitulacao_id( $_POST[ 'titulacao' ] );
					$consultor->setInstituicao_id( $_POST[ 'instituicao' ] );
					$consultor->setLattes( $_POST[ 'lattes' ] );
					$consultor->setEmail( $_POST[ 'email' ] );
					$login = new Login();
					$login->setEmail( $consultor->getEmail() );
					$login->setSenha( $_POST[ 'senha1' ] );
					$login->setTipo( 3 );
					$login->setAtivo( 0 );
					$login->setData( date( "Y-m-d H:i:s" ) );
					$consultaID = $con->prepare( "SELECT * FROM consultor a JOIN login l WHERE a.email=l.email AND l.email=? AND l.senha=?" );
					$consultaID->execute( array( $login->getEmail(), $login->getSenha() ) );
					if ( $consultaID->rowCount() > 0 ) {
						echo "<div class='msg-erro'>Já possui um usuário cadastrado com esse login</div>";
					} else {
						$prepareCons = $con->prepare( "INSERT INTO consultor(nome,email,instituicao_id,titulacao_id,cidade_id,estado_id,pais_id,lattes)VALUES(:nome,:email,:instituicao_id,:titulacao_id,:cidade_id,:estado_id,:pais_id,:lattes) " );
						$prepareCons->execute( $consultor->getConsultor() );

						$prepareLogin = $con->prepare( "INSERT INTO login(email,senha,data,tipo,ativo)VALUES(:email,:senha,:data,:tipo,:ativo) " );
						$prepareLogin->execute( $login->getLogin() );
						//resgatar ide do consultor
						$consultaID = $con->prepare( "SELECT * FROM consultor a JOIN login l WHERE a.email=l.email AND l.email=? AND l.senha=?" );
						$consultaID->execute( array( $login->getEmail(), $login->getSenha() ) );
						$resultadoID = $consultaID->fetch();
						foreach ( $_POST[ 'area' ] as $area ) {

							$insereArea = $con->prepare( "INSERT INTO consultor_area(consultor_id,area_id) VALUES(?,?)" );
							$insereArea->execute( array( $resultadoID[ 0 ], $area ) );
						}
						foreach ( $_POST[ 'subarea' ] as $subarea ) {

							$insereArea = $con->prepare( "INSERT INTO consultor_subarea(consultor_id,subarea_id) VALUES(?,?)" );
							$insereArea->execute( array( $resultadoID[ 0 ], $subarea ) );
						}
					}

					if ( $prepareCons->rowCount() > 0 && $prepareLogin->rowCount() > 0 ) {
						echo "<div class='msg-confimacao'>Agradecemos seu cadastro, após analise das informações prestadas tera seu acesso liberado</div>";

						//importa o phpmailer
						require '../classes/PHPMailerAutoload.php';

						//define o usuario e senha do gmail
						define( 'GUSER', "avaliacao.fetec@gmail.com" ); // <-- Insira aqui o seu GMail
						define( 'GPWD', "AvAFeteCMs" ); // <-- Insira aqui a senha do seu GMail

						//mensagem a ser enviada
						$Vai = "Agradecemos seu Cadastro como Avaliador Online na 7 ª Edição da FETECMS,  aguarde novo contato em breve,  pedimos que nos ajude a divulgar junto aos seus colegas compartilhando esta URL: http://avaliador.grupoarandums.com.br/cadastro.php";

						//funcao de envio do email
						function enviarEmail( $para, $de, $de_nome, $assunto, $corpo ) {
							global $error;
							$mail = new PHPMailer();
							$mail->IsSMTP(); // Ativar SMTP
							$mail->SMTPDebug = 0; // Debugar: 1 = erros e mensagens, 2 = mensagens apenas
							$mail->SMTPAuth = true; // Autenticação ativada
							$mail->SMTPSecure = 'tls'; // SSL REQUERIDO pelo GMail
							$mail->Host = 'smtp.gmail.com'; // SMTP utilizado
							$mail->Port = 587; // A porta 587 deverá estar aberta em seu servidor
							$mail->Username = GUSER;
							$mail->Password = GPWD;
							$mail->SetFrom( $de, $de_nome );

							$mail->Subject = $assunto;
							$mail->Body = $corpo;
							$mail->AddAddress( $para );

							$mail->CharSet = 'utf-8';

							if ( !$mail->Send() ) {
								$error = 'Mail error: ' . $mail->ErrorInfo;

								return false;
							} else {
								return true;
							}
						}

						enviarEmail( $_POST[ 'email' ], "avaliacao.fetec@gmail.com", 'AVALIAÇÃO FETECMS', 'Cadastro Realizado', $Vai );

					} else {
						echo "<div class='msg-erro'>Erro</div>";
					}
				}

				?>