<?php
					$botao = isset( $_POST[ 'atualizar' ] ) ? $_POST[ 'atualizar' ] : '';

					if ( $botao == "Atualizar" ) {

						$consultor = new Consultor();
						$consultor->setNome( $_POST[ 'nome' ] );
						$consultor->setPais_id( intval($_POST[ 'pais' ] ));
						$consultor->setEstado_id( intval($_POST[ 'estado' ]) );
						$consultor->setCidade_id(intval($_POST[ 'cidade' ]) );
						$consultor->setTitulacao_id( intval($_POST[ 'titulacao' ] ));
						$consultor->setInstituicao_id( intval($_POST[ 'instituicao' ]) );
						$consultor->setLattes( $_POST[ 'lattes' ] );
						$consultor->setEmail( $_POST[ 'email' ] );
						
						$login = new Login();
						$login->setEmail( $consultor->getEmail() );
					
                        if( isset($_POST[ 'atualizaSenha' ]) && crip($_POST[ 'senhaAtual' ])==SENHA ){
                          $login->setSenha( $_POST[ 'senha1' ] );  
                        }
                        else if(!isset($_POST[ 'atualizaSenha' ])){
                           $login->setSenha(descrip(SENHA));   
                        }
                        else{
                        	echo "<div class='msg-erro'>Senha Atual incorreta</div>";
                        	return;
                        }
						$login->setTipo( 3 );
						$login->setData( date( "Y-m-d H:i:s" ) );
                        $login->setAtivo(1);
						$prepareCons = $con->prepare( "UPDATE consultor SET nome=:nome,email=:email,instituicao_id=:instituicao_id,titulacao_id=:titulacao_id,cidade_id=:cidade_id,estado_id=:estado_id,pais_id=:pais_id,lattes=:lattes WHERE id_consultor=" . ID_USU . " " );
						$prepareCons->execute( $consultor->getConsultor() );
						$prepareLogin = $con->prepare( "UPDATE login SET email=:email,senha=:senha,data=:data,tipo=:tipo,ativo=:ativo WHERE id_login=" . ID_USUL . "" );
						$prepareLogin->execute( $login->getLogin() );

						if ( $prepareCons && $prepareLogin ) {

							$_SESSION[ 'usuario_login' ][ 'email' ] = $consultor->getEmail();
							$_SESSION[ 'usuario_login' ][ 'senha' ] = $login->getSenha();
						

							echo "<div class='msg-confimacao'>Atualizado com Sucesso!</div>";

						} else {
							echo "<div class='msg-erro'>Erro</div>";
						}
					}

					?>