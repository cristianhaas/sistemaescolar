<?php 
include_once("../model/modal.php");
include_once("../config/sistema.php" );
$id_administrador=$_POST['id_administrador'];
$preparaConsutaAdm=$con->prepare("SELECT * FROM chf_administrador, chf_login WHERE chf_login.email=chf_administrador.email AND chf_administrador.id_administrador=?");
$preparaConsutaAdm->execute(array($id_administrador));
$administrador=$preparaConsutaAdm->fetch();
$modal=new Modal;
$modal->setTitulo("Confirmação");
$modal->setTituloTexto("Deseja remover esse usuário?");
$modal->setTexto($administrador['nome']);
$modal->setTextoBotaoConfirmar("Remover");
$modal->setAcao("confirmaRemoverUsuario($id_administrador)");
$modal->show();

?>