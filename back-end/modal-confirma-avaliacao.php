<?php 
include_once("../model/modal.php");
include_once("../config/sistema.php" );
$id_projeto=$_POST['projeto'];
$preparaConsultaProjeto=$con->prepare("SELECT titulo FROM projeto WHERE id_projeto=?");
$preparaConsultaProjeto->execute(array($id_projeto));
$projeto=$preparaConsultaProjeto->fetch();
$modal=new Modal;
$modal->setTitulo("Confirmação");
$modal->setTituloTexto("Deseja solicitar a avaliação desse projeto?");
$modal->setTexto($projeto['titulo']);
$modal->setTextoBotaoConfirmar("Solicitar");
$modal->setAcao("confirmaSolicitarAvaliacao($id_projeto)");
$modal->show();

?>