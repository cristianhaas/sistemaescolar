
<?php
session_start();
include_once( "config/funcoes.php" );
include_once( "config/sistema.php" );

if ( isset( $_GET[ 'sair' ] ) && $_GET[ 'sair' ] == true ) {
	include_once( "back-end/deslogar.php" );
}
include_once( "model/view.class.php" );
if ( isset( $_SESSION[ 'usuario_login' ] ) )
	include_once( "view/index.php" );
else if ( !isset( $_SESSION[ 'usuario_login' ] ) && $categoria!="cadastro" )
	include_once( "view/login.php" );
else if ( ( isset( $categoria ) && file_exists( "view/$categoria.php" ) ) )
	include_once( "view/$categoria.php" );
else if ( isset( $subcategoria ) && file_exists( "view/$subcategoria.php" ) )
	include_once( "view/$subcategoria.php" );

?>