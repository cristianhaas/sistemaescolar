<?php

define( "BASE", 'http://localhost/Escolar/' );
define( "SITE", 'Sistema Escolar' );
define( "BD_SISTEMA", 'chf_sistema_escolar' );
//define( "BD_SISTEMA", 'u446320541_sise' );
	
header( "Content-type: text/html; charset=utf-8" );

function Conecta() {
	 $conexao = new PDO( "mysql:host=localhost;dbname=" . BD_SISTEMA . ";charset=UTF8", 'root', '' );
	// $conexao = new PDO( "mysql:host=mysql.hostinger.com.br;dbname=" . BD_SISTEMA . ";charset=UTF8", 'u446320541_usise', 'I4Gv17wFoGS5' );
	return $conexao;
}

$categoria = '';
$pagina = '';
if ( isset( $_GET[ 'url' ] ) ) {
	$url = $_GET[ 'url' ];
	$quebraUrl = explode( '/', $url );

	$pagina = isset( $quebraUrl[ 0 ] ) ? $quebraUrl[ 0 ] : '';
	$categoria = isset( $quebraUrl[ 1 ] ) ? $quebraUrl[ 1 ] : '';
}

$con = Conecta();
if ( isset( $_SESSION[ 'usuario_login' ] ) && $_SESSION[ 'usuario_login' ] != null && $_SESSION[ 'usuario_login' ][ 'tipo' ] == 3 ) {
	$preparaLogin = $con->prepare( "SELECT c.nome,l.senha,c.email,l.foto,l.tipo,i.nome AS instituicao, c.lattes, t.titulo,e.nome AS estado,ci.nome AS cidade, p.Nome AS pais, l.id_login,c.id_consultor FROM login l, consultor c, instituicao i, titulacao t, cidade ci, estado e,pais p  WHERE l.email=? AND l.senha=? AND c.email=l.email AND c.titulacao_id=t.id AND c.instituicao_id=i.id AND ci.id=c.cidade_id AND e.id=c.estado_id" );
	$preparaLogin->execute( array( $_SESSION[ 'usuario_login' ][ 'email' ], $_SESSION[ 'usuario_login' ][ 'senha' ] ) );
	$avaliador = $preparaLogin->fetch();
	define( "ID_USU",  $avaliador[ 'id_consultor' ] );
    define( "ID_USUL",  $avaliador[ 'id_login' ] );
	define( "NOME",  $avaliador[ 'nome' ] );
	define( "EMAIL", $avaliador[ 'email' ] );
	define( "TIPO", $avaliador[ 'tipo' ] );
	define( "FOTO", $avaliador[ 'foto' ] );
	define( "LATTES", $avaliador[ 'lattes' ] );
	define( "INSTITUICAO", $avaliador[ 'instituicao' ] );
	define( "TITULACAO", $avaliador[ 'titulo' ] );
	define( "CIDADE", $avaliador[ 'cidade' ] );
	define( "ESTADO", $avaliador[ 'estado' ] );
	define( "PAIS", $avaliador[ 'pais' ] );
    define( "SENHA", $avaliador[ 'senha' ] );


} else if ( isset( $_SESSION[ 'usuario_login' ] ) && $_SESSION[ 'usuario_login' ] != null && $_SESSION[ 'usuario_login' ][ 'tipo' ] == 1 ) {
	$preparaLogin = $con->prepare( "SELECT a.nome,a.email,l.foto,l.tipo,l.senha,l.id_login,a.id_administrador,l.id_escola FROM chf_login l, chf_administrador a WHERE l.email=? AND l.senha=? AND a.email=l.email" );
	$preparaLogin->execute( array( $_SESSION[ 'usuario_login' ][ 'email' ], $_SESSION[ 'usuario_login' ][ 'senha' ] ) );
	$administrador = $preparaLogin->fetch();
	define( "ID_USU",  $administrador[ 'id_administrador' ] );
    define( "ID_USUL",  $administrador[ 'id_login' ] );
    define("ID_ESCOLA",$administrador['id_escola']);
	define( "NOME",  $administrador[ 'nome' ] );
	define( "EMAIL", $administrador[ 'email'] );
	define( "TIPO",  $administrador[ 'tipo' ] );
	define( "FOTO",  $administrador[ 'foto' ] );
	define( "SENHA",  $administrador[ 'senha' ] );

}

?>