<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3></h3>
			</div>

			<div class="title_right">
				<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search for...">
						<span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
					


					</div>
				</div>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">

			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Usuários <small>Inclui usuário com acesso Administrador ao sistema</small></h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Settings 1</a>
									</li>
									<li><a href="#">Settings 2</a>
									</li>
								</ul>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
           
						<!-- start form for validation -->
						<form id="formulario-padrao"  data-parsley-validate action="" method="POST" onsubmit="return false">
							<label for="fullname">Escola *</label>
							<select class="form-control" name="escola" required>
								<?php 
								$preparaConsultaEscola=$con->prepare("SELECT * FROM chf_escola");
								$preparaConsultaEscola->execute();
								$resultado=$preparaConsultaEscola->fetchAll();
								foreach ($resultado as $escola) {	
								 ?>
								<option value="<?php echo $escola['id_escola'] ?>"><?php echo $escola['nome']?></option>
								<?php }?>
							</select>

							<label for="fullname">Nome Completo *:</label>
							<input type="text" id="nome" class="form-control" name="nome" required />

							<label for="email">Email * :</label>
							<input type="email" id="email" class="form-control" name="email" data-parsley-trigger="change" required/>
							<label for="cargo">Cargo * :</label>
							<input type="text" id="cargo" class="form-control" name="cargo" data-parsley-trigger="change" required/>

							<label for="senha">Senha * :</label>
							<input type="password" id="senha" class="form-control" name="senha" data-parsley-trigger="change" required/>
                             

							<button type="submit" value="Cadastrar" id="cadastrar-usuario" name="cadastrar-usuario" class="btn btn-primary">Cadastrar</button>

							
						</form>
						<!-- end form for validations -->
                        <!-- ### acção do formulario-->
                        <?php include_once("back-end/incluir-usuarios-global.php"); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>