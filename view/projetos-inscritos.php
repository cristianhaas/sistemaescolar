<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Users <small>Some examples to get you started</small></h3>
			</div>

			<div class="title_right">
				<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search for...">
						<span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
					
					</div>
				</div>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">
		
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Button Example <small>Users</small></h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Settings 1</a>
									</li>
									<li><a href="#">Settings 2</a>
									</li>
								</ul>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<p class="text-muted font-13 m-b-30">
Abaixo segue a lista de todos os projetos inscritos. Caso queira avaliar um que náo esteja em sua lista de avaliação, clique em "Solicitar avaliação" para o responsável pela Comissão de Avaliação liberar.
						</p>
						<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive">
							<thead>
								<tr>
									<th>Titulo </th>
								
								</tr>
							</thead>


							<tbody>
							<?php 
							$con=ConectaSemUTF();
							$consultaProjetos= $con->query("SELECT * FROM projeto");
							$projetos=$consultaProjetos->fetchAll();
								foreach($projetos as $projeto){
								?>
								<tr>
									<td><?php echo $projeto['titulo']; ?><input type="button" class="btn btn-info solicitar-avaliacao" style="float: right" id="<?php echo $projeto['id_projeto']; ?>" value="Solicitar avaliação"></td>
									
								</tr>
								<?php }?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>