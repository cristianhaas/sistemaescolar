<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3></h3>
			</div>

			<div class="title_right">
				<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search for...">
						<span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
					

					</div>
				</div>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">

			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Usuários <small>Lista de usuários com acesso Administrador ao sistema</small></h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Settings 1</a>
									</li>
									<li><a href="#">Settings 2</a>
									</li>
								</ul>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<p class="text-muted font-13 m-b-30">
					
						</p>
						<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive">
							<thead>
								<tr>
									<th>Nome </th>
									<th>Email </th>
									<th>Escola </th>
								</tr>
							</thead>


							<tbody>
								<?php 
							$consultaUsuarios=$con->prepare("SELECT chf_administrador.*,chf_escola.nome AS nome_escola FROM chf_administrador LEFT JOIN chf_login ON chf_login.email=chf_administrador.email LEFT JOIN chf_escola ON chf_login.id_escola=chf_escola.id_escola GROUP BY chf_login.id_login");
							$consultaUsuarios->execute();
							$administradores=$consultaUsuarios->fetchAll();
								foreach($administradores as $admin){
								?>
								<tr>
									<td id='us<?php echo $admin['id_administrador']; ?>'> 
										<?php if($view->getView()){?><button type="button" class="btn btn-round btn-success fa fa-eye"></button><?php } ?>
										<?php if($view->getEdit()){?><button type="button" class="btn btn-round btn-warning fa fa-edit"></button><?php } ?>
										<?php if($view->getDelete()){?><button type="button" class="btn btn-round btn-danger fa fa-remove remover-usuario" id="<?php echo $admin['id_administrador']; ?>"></button><?php } ?>
										<?php echo $admin['nome']; ?> 
									</td>
									<td> 
						
										<?php echo $admin['email']; ?> 
									</td>
									<td> 
						
										<?php echo $admin['nome_escola']; ?> 
									</td>
								</tr>
								<?php }?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>