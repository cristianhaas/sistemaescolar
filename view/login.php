<?php include_once("config/sistema.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo SITE ?> - Login</title>

	<!-- Bootstrap -->
	<link href="<?php echo BASE; ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="<?php echo BASE; ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- NProgress -->
	<link href="<?php echo BASE; ?>vendors/nprogress/nprogress.css" rel="stylesheet">
	<!-- Animate.css -->
	<link href="<?php echo BASE; ?>vendors/animate.css/animate.min.css" rel="stylesheet">

	<!-- Custom Theme Style -->
	<link href="<?php echo BASE; ?>build/css/custom.css" rel="stylesheet">
	<!-- Estilo css personalizado -->
	<link rel="stylesheet" href="<?php echo BASE; ?>css/estilo-personalizado.css">
</head>

<body class="login" style="background:url(<?php echo BASE; ?>img/fundo.jpg) center center; background-size: 100% 200% ">
	<div> <a class="hiddenanchor" id="signup"></a> <a class="hiddenanchor" id="signin"></a>
		<div class="login_wrapper" >
			<div class="animate form login_form" style="background-color: rgba(255,255,255,0.8); padding: 10px 20px">

				<section class="login_content">
					<form onSubmit="return false" method="post" id="formLogin">
						<h1>Login </h1>
						<span id="mensagem">
							<!-- Pagina responsável por logar -->
							<?php include_once("back-end/login.php");?>
						</span>
						<div>
							<input type="email" class="form-control" placeholder="Email" name="email" required=""/>
						</div>
						<div>
							<input type="password" class="form-control" placeholder="Senha" name="senha" required=""/>
						</div>
						<div> <button type="submit" class="btn btn-primary submit form-control col-md-7 col-xs-12" id="bttLogin" name="bttLogin" value="Logar">Log in</button> <a class="reset_pass" href="#">Esqueceu sua senha?</a> </div>
						<div class="clearfix"></div>
						<div class="separator">
							<p class="change_link">Novo aqui? <a href="<?php echo BASE."cadastro"?>" class="to_register">Cadastrar!</a> </p>
							<div class="clearfix"></div>
							<br/>
							<div>
								<p>Cristian Haas</p>
							</div>
						</div>
					</form>
				</section>
			</div>

		</div>
	</div>
</body>

</html>
<!-- jQuery  -->
<script src="<?php echo BASE; ?>vendors/jquery/dist/jquery.min.js"></script>

<script>
	$( "#bttLogin" ).on( "click", function ( e ) {

		var $cont = 0;
		var $msg = "";
		//verifica se os campos estao preenchidos
		$( "input:not([type=checkbox])" ).each( function () {
			if ( $( this ).val() == "" ) {
				$cont++;
				$msg = "Preencha todos os campos";
				$( this ).attr( "style", "border:solid 1px #f00;" );
			} else {
				$( this ).attr( "style", "border:1px solid #c8c8c8" );
			}
		} );
		if ( $cont == 0 ) {

			$( "#formLogin" ).removeAttr( "onSubmit" );
			$( "#bttLogin" ).submit();
		} else {
			var body = $( "html, body" );
			body.stop().animate( {
				scrollTop: 0
			}, 500, 'swing' );

			$( "#mensagem" ).html( "<div class='msg-erro'>" + $msg + "</div>" );
		}
	} );
</script>