<div class='right_col' role='main'>
	<div>
		<div class="col-md-12">
			<div class="col-middle">
				<div class="text-center text-center">
					<h1 class="error-number">404</h1>
					<h2>Desculpe, não conseguimos localizar esta página</h2>
					<p>Essa página foi removida ou não existe! <a href="#">Comunicar erro?</a>
					</p>
					<div class="mid_center">
						<h3>Buscar</h3>
						<form>
							<div class="col-xs-12 form-group pull-right top_search">
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Pesquisar...">
										<span class="input-group-btn">
                            		  		<button class="btn btn-default" type="button">Buscar</button>
                        			    </span>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>