<?php
include( "classes/Consultor.class.php" );
include( "classes/Login.class.php" );
?>
<style>
	.msg-erro {
		background-color: rgba(233, 38, 41, 1.00);
		border: solid 2px #A40407;
		border-radius: 4px;
		width: 100%;
		padding: 5px;
		color: #fff;
		text-align: center;
	}
	
	.msg-confimacao {
		background-color: rgba(37, 233, 66, 1.00);
		border: solid 2px rgba(24, 106, 26, 1.00);
		border-radius: 4px;
		width: 100%;
		padding: 5px;
		color: #fff;
		text-align: center;
	}
	
	.form-control {
		border-bottom: solid 2px rgba(199, 199, 199, 1.00);
		height: 40px
	}
	
	.form-control:focus {
		border-bottom: solid 2px rgba(59, 146, 217, 1.00);
	}
    #boxSenha{
    padding: 20px 10px;
    border:solid 1px #ccc;
    border-radius: 2px;
        display: block;
}
</style>
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Meu Perfil </h3>
              </div>
            <div class="clear"></div>

<span id="mensagem">
    <?php include_once("back-end/edita-perfil.php"); ?>
</span>
				<form action="" method="post" id="perfil" onSubmit="return false">
					<div class="form-group has-feedback">
                    Nome completo:
						<input type="text" class="form-control" placeholder="Nome" name="nome" value="<?php echo NOME;?>">
				
					</div>
	               <div class="form-group has-feedback">
                    País:
						<?php echo " <select class='form-control' name='pais' id='pais'>";

                    $preparaConsulta = $con->prepare("SELECT * FROM pais WHERE nome=?");
                    $preparaConsulta->execute(array(PAIS));
                    $select = $preparaConsulta->fetchAll();
                    foreach ($select as $pais) {
                        ?>
						<option <?php echo (PAIS==$pais[1])? "selected ": ''; ?>value="<?php echo $pais[0] ?>">
							<?php echo $pais[1]; ?>
						</option>
						<?php
						}
						echo "</select>";
						?>
						<span class="glyphicon glyphicon-map-marker form-control-feedback"></span>

					</div>
					<div class="form-group has-feedback">
                    Estado:
						<select class='form-control' name='estado' id='estado'>
							<?php
							$preparaConsulta = $con->prepare( "SELECT * FROM estado WHERE nome=?" );
							$preparaConsulta->execute(array(ESTADO));
							$select = $preparaConsulta->fetchAll();
							foreach ( $select as $estado ) {
								?>
							<option <?php echo (ESTADO==$estado[1])? " selected ": '';?> value="<?php echo $estado[0] ?>">
								<?php echo $estado[1]; ?>
							</option>
							<?php
							}
							?>
						</select>
						<span class="glyphicon glyphicon-map-marker form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
                        Cidade:
						<select class='form-control' name='cidade' id='cidade'>
							<?php
							$preparaConsulta = $con->prepare( "SELECT * FROM cidade WHERE nome=?" );
							$preparaConsulta->execute(array(CIDADE));
							$select = $preparaConsulta->fetchAll();
							foreach ( $select as $cidade ) {
								?>
							<option <?php echo (CIDADE==$cidade[1])? " selected ": '';?> value="<?php echo $cidade[0] ?>">
								<?php echo $cidade['nome']; ?>
							</option>
							<?php
							}
							?>
						</select>
						<span class="glyphicon glyphicon-map-marker form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
                    Instituição
						<?php echo " <select class='form-control' name='instituicao' id='instituicao'><option value='0'>Instituição</option>";

                    $preparaConsulta = $con->query("SELECT * FROM instituicao");
                    $select = $preparaConsulta->fetchAll();
                    foreach ($select as $instituicao) {
                        ?>
						<option <?php echo (INSTITUICAO==$instituicao[1])? " selected ": '';?> value="
							<?php echo $instituicao[0] ?>">
							<?php echo $instituicao[1]; ?>
						</option>
						<?php
						}
						echo "</select>";
						?>
						<span class="glyphicon glyphicon-book form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
                    Titulação:

						<?php echo " <select class='form-control' name='titulacao' id='titulacao'><option value='0'>Titulação</option>";

                    $preparaConsulta = $con->query("SELECT * FROM titulacao");

                    $select = $preparaConsulta->fetchAll();
                    foreach ($select as $titulacao) {
                        ?>
						<option<?php echo (TITULACAO==$titulacao[1])? " selected ": '';?> value="<?php echo $titulacao[0] ?>">
							<?php echo $titulacao[1]; ?>
							</option>
							<?php
							}
							echo "</select>";
							?>
							<span class="glyphicon glyphicon-education form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
                        Currículo Lattes:
						<input type="text" class="form-control" placeholder="Lattes" name="lattes" value="<?php echo LATTES; ?>">
						<span class="glyphicon glyphicon-link form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
                        Email:
						<input type="email" class="form-control" placeholder="Email" name="email" id="email" value="<?php echo EMAIL;?>">
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
					</div>
                       <div class="clear"></div>
                    <div id="boxSenha" class="form-group has-feedback" >

                        <input type="checkbox" id="atualizarSenha" name="atualizaSenha" value="0"> Atualizar senha
					<div class="form-group has-feedback">
                        Nova Senha:
						<input type="password" class="form-control" placeholder="Senha" name="senha1" id="senha1" value="" disabled>
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>			<div class="form-group has-feedback">
                        Confirmar nova senha:
						<input type="password" class="form-control" placeholder="Repetir senha" name="senha2" id="senha2" value="" disabled>
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div><div class="form-group has-feedback">

                        Senha Atual:
						<input type="password" class="form-control" placeholder="Digite sua senha atual para confirmar" name="senhaAtual" id="senhaAtual" value="" disabled>
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
			
                    </div>
                    </div>
					<div class="row">
						<!-- /.col -->
						<div class="col-xs-4">
							<button type="submit" class="btn btn-primary btn-block btn-flat" name="atualizar" id="atualizar" value="Atualizar">Atualizar</button>
						</div>
						<!-- /.col -->
					</div>
				</form>
				

		
              </div>
            </div>
    </div>

<script src="<?php echo BASE; ?>vendors/jquery/dist/jquery.min.js"></script>

	<script>
     $("#atualizarSenha").on("click",function(){
         if($(this).val()==0){
            $(this).val(1) ;
         $("#boxSenha input[type=password]").removeAttr("disabled");
         }else{
             $(this).val(0);
        $("#boxSenha input[type=password]").attr("disabled","disabled");  
         }
     });

$("#pais").on("click",function(){    
    if($("#pais option").not(":disabled").length==1 && $("#pais option:selected").val()!=0){
      $( "#pais" ).html( "<option selected value='0'>Carregando...</option>" );
      $.get("<?php echo BASE;?>requisicao/all-pais.php",function(data){
         setTimeout(function(){  $( "#pais option" ).trigger('click'); $( "#pais" ).html( data )},2000);
      });
    }
});
function buscaEstados(){
        	$( "#cidade" ).html( "<option selected value='0'></option>" );
			$.ajax( {
				type: "POST",
				url: "<?php echo BASE;?>requisicao/altera-pais.php",
				dataType: 'html',
				data: ( {
					pais: $( "#pais option:selected" ).val()
				} ),
				beforeSend: function ( data ) {},
				success: function ( data ) {
					$( "#estado" ).html( data )
				},
				complete: function ( data ) {}
			} );
}
        function buscaCidades(){
            	$.ajax( {
				type: "POST",
				url: "<?php echo BASE;?>requisicao/altera-estado.php",
				dataType: 'html',

				data: ( {
					estado: $( "#estado option:selected" ).val()
				} ),
				beforeSend: function ( data ) {},
				success: function ( data ) {
					$( "#cidade" ).html( data );
					
				},
				complete: function ( data ) {}
			} );


        }
$("#estado").on("click",function(){   
  
    if($("#estado option").not(":disabled").length==1){
          $("#estado option:selected").html("Carregando...");
        setTimeout(function(){  buscaEstados()},2000);
        }
});
		$( "#pais" ).on( 'change', function () {
            buscaEstados();
		} );
     $("#cidade").on("click",function(){   
  
    if($("#cidade option").not(":disabled").length==1){
          $("#cidade option:selected").html("Carregando...");
        setTimeout(function(){  buscaCidades()},2000);
        }
}); 
		$( "#estado" ).on( 'change', function () {
			$.ajax( {
				type: "POST",
				url: "<?php echo BASE;?>requisicao/altera-estado.php",
				dataType: 'html',

				data: ( {
					estado: $( this ).val()
				} ),
				beforeSend: function ( data ) {},
				success: function ( data ) {
					$( "#cidade" ).html( data );
					$
				},
				complete: function ( data ) {}
			} );
	$( "#instituicao" ).html("");

$.ajax( {
				type: "POST",
				url: "<?php echo BASE;?>requisicao/altera-instituicao.php",
				dataType: 'html',

				data: ( {
					estado: $( this ).val()
				} ),
				beforeSend: function ( data ) {},
				success: function ( data ) {
					$( "#instituicao" ).html( data )
				},
				complete: function ( data ) {}
			} );
		} );
        
        
  //confirmar      
        		$( "#atualizar" ).on( "click", function ( e ) {

			var $cont = 0;
			var $msg = "";
			//verifica se os campos estao preenchidos
			$( ".form-group input:not([type=checkbox],[disabled=disabled])" ).each( function () {
				if ( $( this ).val() == "" ) {
					$cont++;
					$msg = "Preencha todos os campos";
					$( this ).attr( "style", "border:solid 1px #f00;" );
				} else {
					$( this ).attr( "style", "border:1px solid #c8c8c8" );
				}
			} );
			//verifica se os selects  forem selecionados
			$( ".form-group select" ).each( function () {
				if ( $( this ).val() == 0) {
					$msg = "Preencha todos os campos";
					$( this ).attr( "style", "border:solid 1px #f00;" );
					$cont++;
				} else {
					$( this ).attr( "style", "border:1px solid #c8c8c8" );
				}

			} );
			
			
		
			
			//verifica se os emails correspondem
			if ( $( "#senha1" ).val() != $( "#senha2" ).val() && $msg=="") {
				$cont++;
				$msg = "As senhas não correspondem";
			}


			//caso nao tiver erro
			if ( $cont == 0 ) {
                $( "#mensagem" ).html( "" );	
				$( "#perfil" ).removeAttr( "onSubmit" );
				$( "#perfil" ).submit();
			} else {
			var body = $("html, body");
body.stop().animate({scrollTop:0}, 500, 'swing');

				$( "#mensagem" ).html( "<div class='msg-erro'>" + $msg + "</div>" );	
			}
		} );
</script>