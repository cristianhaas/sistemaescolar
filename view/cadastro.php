﻿<?php
include_once( "config/funcoes.php" );
include_once( "config/sistema.php" );
include_once( "classes/Login.class.php" );
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>
		<?php echo SITE;?> | Cadastro</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="<?php echo BASE; ?>vendors/bootstrap/dist/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Check BOx -->
	<link rel="stylesheet" href="<?php echo BASE; ?>vendors/iCheck/skins/square/blue.css">
	<!-- Estilo css personalizado -->
	<link rel="stylesheet" href="<?php echo BASE; ?>css/estilo-personalizado.css">

</head>
<body class="hold-transition login-page" style="background:url(<?php echo BASE; ?>img/fundo.jpg) center center; background-size: 100% 200% ">
	<div class="login-box">

		<!-- /.login-logo -->
		<div class="login-box-body">
			<div class="login-logo">
				<a href="<?php echo BASE; ?>"><img src="<?php echo BASE; ?>img/feteclogo2.png"></a>
			</div>
			<p class="login-box-msg">Realize seu cadastro para ter acesso ao sistema</p>
<span id="mensagem">
<!-- Pagina responsável por cadastrar avaliador-->
		<?php include_once("back-end/cadastro-avaliador.php");?>
			</span>
			<form action="" method="post" id="cadastro" onSubmit="return false">
				<div class="form-group has-feedback">
					<input type="text" class="form-control" placeholder="Nome" name="nome" value="">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="email" class="form-control" placeholder="Email" name="email" id="email" value="">
					<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="password" class="form-control" placeholder="Senha" name="senha1" id="senha1" value="">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="password" class="form-control" placeholder="Repetir senha" name="senha2" id="senha2" value="">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>	
				<div class="row">
					<div class="col-xs-8">
						<div class="checkbox icheck">

						</div>
					</div>
					<!-- /.col -->
					<div class="col-xs-4">
						<button type="submit" class="btn btn-primary btn-block btn-flat" name="cadastrar" id="cadastrar" value="Cadastrar">Cadastrar</button>
					</div>
					<!-- /.col -->
				</div>
			</form>
			

			<!-- /.social-auth-links -->

			<a href="#"></a><br>


		</div>
		<!-- /.login-box-body -->
	</div>
	<!-- /.login-box -->

	<!-- jQuery  -->
	<script src="<?php echo BASE; ?>vendors/jquery/dist/jquery.min.js"></script>

	<script>
	
		$( "#cadastrar" ).on( "click", function ( e ) {

			var $cont = 0;
			var $msg = "";
			//verifica se os campos estao preenchidos
			$( ".form-group input:not([type=checkbox])" ).each( function () {
				if ( $( this ).val() == "" ) {
					$cont++;
					$msg = "Preencha todos os campos";
					$( this ).attr( "style", "border:solid 1px #f00;" );
				} else {
					$( this ).attr( "style", "border:1px solid #c8c8c8" );
				}
			} );
			//verifica se os selects  forem selecionados
			$( ".form-group select" ).each( function () {
				if ( $( this ).val() == 0) {
					$msg = "Preencha todos os campos";
					$( this ).attr( "style", "border:solid 1px #f00;" );
					$cont++;
				} else {
					$( this ).attr( "style", "border:1px solid #c8c8c8" );
				}

			} );
		
			//validar email
			$.ajax( {
				type: "POST",
				async: false,
				url: "<?php echo BASE ?>requisicao/verifica-email.php",
				dataType: 'html',

				data: ( {
					email: $( "#email" ).val()
				} ),
				beforeSend: function ( data ) {},
				success: function ( data ) {

					if ( data != true ) {
						if($cont==0 && $msg=="" ){
							$msg = data;
						}		
						$cont++;
						


					}
				},
				complete: function ( data ) {}
			} );



			//caso nao tiver erro
			if ( $cont == 0 ) {

				$( "#cadastro" ).removeAttr( "onSubmit" );
				$( "#cadastro" ).submit();
			} else {
			var body = $("html, body");
body.stop().animate({scrollTop:0}, 500, 'swing');

				$( "#mensagem" ).html( "<div class='msg-erro'>" + $msg + "</div>" );	
			}
		} );

	</script>
	<script src="<?php echo BASE; ?>vendors/iCheck/icheck.min.js"></script>
<script>

</script>
</body>

</html>