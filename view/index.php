<!DOCTYPE html>
<html lang="en">
 <head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Sistema Escolar</title>
 

	<!-- Bootstrap -->
	<link href="<?php echo BASE; ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="<?php echo BASE; ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- NProgress -->
	<link href="<?php echo BASE; ?>vendors/nprogress/nprogress.css" rel="stylesheet">
	<!-- iCheck -->
	<link href="<?php echo BASE; ?>vendors/iCheck/skins/flat/green.css" rel="stylesheet">

	<!-- bootstrap-progressbar -->
	<link href="<?php echo BASE; ?>vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
	<!-- JQVMap -->
	<link href="<?php echo BASE; ?>vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
	<!-- bootstrap-daterangepicker -->
	<link href="<?php echo BASE; ?>vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

	<!-- Custom Theme Style -->
	<link href="<?php echo BASE; ?>build/css/custom.min.css" rel="stylesheet">
	       <!-- Datatables -->
    <link href="<?php echo BASE;?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo BASE;?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo BASE;?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo BASE;?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo BASE;?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    
       <!-- PNotify solicita avaliacao -->
    <link href="<?php echo BASE; ?>vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="<?php echo BASE; ?>vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">




</head>

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<div class="left_col scroll-view">
					<div class="navbar nav_title" style="border: 0;">
						<a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Escolar</span></a>
					</div>
					<!-- menu profile quick info -->
					<div class="profile clearfix">
						<div class="profile_pic">
							<img src="<?php echo BASE."img/".FOTO."-pequeno.png"; ?>" alt="foto do perfil" class="img-circle profile_img">
						</div>
						<div class="profile_info">
							<span>Bem Vindo,</span>
						
							<h2>
								<?php echo NOME; ?>

							</h2>
						</div>
					</div>
					<!-- /menu profile quick info -->

					<br/>

					<!-- sidebar menu -->
					<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
						<div class="menu_section">
							<h3>Menu</h3>
							<ul class="nav side-menu">
								<?php 
								$SelecionaMenus=$con->query("SELECT * FROM chf_menu");
								$menu=$SelecionaMenus->fetchAll();

								if($SelecionaMenus->rowCount()>0)
								foreach($menu as $itemMenu){
									$preparaConsultaMenu=$con->prepare("SELECT * FROM chf_menu m, chf_tabela_permissoes tp WHERE m.id_menu=tp.id_menu AND tp.tipo_usuario=? AND m.id_menu=?");
									$preparaConsultaMenu->execute(array($_SESSION['usuario_login']['tipo'],$itemMenu['id_menu']));
									$menusSubmenus=$preparaConsultaMenu->fetchAll();
									if($preparaConsultaMenu->rowCount()>0){

								?>
								<li><a><i class="fa <?php echo $itemMenu['icone']?>"></i> <?php echo $itemMenu['nome']?> <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<?php
									
									foreach($menusSubmenus as $submenu){
										if($submenu['visualizar']){?>
											<li><a href="<?php echo BASE.$itemMenu['nome_link']."/visualizar";?>">Visualizar</a>
										</li>
										<?php 
										if($submenu['incluir']){?>
											<li><a href="<?php echo BASE.$itemMenu['nome_link']."/incluir";?>">Incluir</a>
										</li>
										<?php
										}
										?>

									
										<?php }?>
									</ul>
								</li>
								<?php 
									}}
								}?>
						
							</ul>
						</div>
					
						</div>

					</div>
					<!-- /sidebar menu -->

					<!-- /menu footer buttons -->
					<div class="sidebar-footer hidden-small">
						<a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
					
						<a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
					
						<a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
					
						<a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
					
					</div>
					<!-- /menu footer buttons -->
				</div>
			</div>

			<!-- top navigation -->
			<div class="top_nav">
				<div class="nav_menu">
					<nav>
						<div class="nav toggle">
							<a id="menu_toggle"><i class="fa fa-bars"></i></a>
						</div>

						<ul class="nav navbar-nav navbar-right">
							<li class="">
								<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo BASE."img/".FOTO."-pequeno.png"; ?>" alt=""><?php echo NOME; ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
							
								<ul class="dropdown-menu dropdown-usermenu pull-right">
									<li><a href="<?php echo BASE."painel/perfil"; ?>"> Perfil</a>
									</li>
									<li>
										<a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
									
									</li>
									<li><a href="javascript:;">Help</a>
									</li>
									<li><a href="<?php echo BASE."?sair=1"; ?>"><i class="fa fa-sign-out pull-right"></i> Sair</a>
									</li>
								</ul>
							</li>

							<li role="presentation" class="dropdown">
								<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green">6</span>
                  </a>
							
								<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
									<li>
										<a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
									
									</li>
									<li>
										<a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
									
									</li>
									<li>
										<a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
									
									</li>
									<li>
										<a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
									
									</li>
									<li>
										<div class="text-center">
											<a>
                          <strong>See All Alerts</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
										
										</div>
									</li>
								</ul>
							</li>
						</ul>
					</nav>
				</div>
			</div>
			<!-- /top navigation -->
                   <div id="espaco-modal"></div>
			<!-- page principal meio -->
	<script src="<?php echo BASE; ?>vendors/jquery/dist/jquery.min.js"></script>
	   <!-- PNotify -->
    <script src="<?php echo BASE; ?>vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?php echo BASE; ?>vendors/pnotify/dist/pnotify.buttons.js"></script>
			<?php 
			$view = new View($con,$pagina,$categoria,TIPO,ID_ESCOLA);
			include_once($view->show());


		  	?>
			<!-- /page  principal meio-->

			<!-- footer content -->
			<footer>
				<div class="pull-right">
					Cristian Haas Fretes - Engenheiro de Software</a>
				</div>
				<div class="clearfix"></div>
			</footer>
			<!-- /footer content -->
		</div>
	</div>


	<!-- Bootstrap -->
	<script src="<?php echo BASE; ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="<?php echo BASE; ?>vendors/fastclick/lib/fastclick.js"></script>
	<!-- NProgress -->
	<script src="<?php echo BASE; ?>vendors/nprogress/nprogress.js"></script>
	<!-- Chart.js -->
	<script src="<?php echo BASE; ?>vendors/Chart.js/dist/Chart.min.js"></script>
	<!-- gauge.js -->
	<script src="<?php echo BASE; ?>vendors/gauge.js/dist/gauge.min.js"></script>
	<!-- bootstrap-progressbar -->
	<script src="<?php echo BASE; ?>vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
	<!-- iCheck -->
	<script src="<?php echo BASE; ?>vendors/iCheck/icheck.min.js"></script>
	<!-- Skycons -->
	<script src="<?php echo BASE; ?>vendors/skycons/skycons.js"></script>
	<!-- Flot -->
	<script src="<?php echo BASE; ?>vendors/Flot/jquery.flot.js"></script>
	<script src="<?php echo BASE; ?>vendors/Flot/jquery.flot.pie.js"></script>
	<script src="<?php echo BASE; ?>vendors/Flot/jquery.flot.time.js"></script>
	<script src="<?php echo BASE; ?>vendors/Flot/jquery.flot.stack.js"></script>
	<script src="<?php echo BASE; ?>vendors/Flot/jquery.flot.resize.js"></script>
	<!-- Flot plugins -->
	<script src="<?php echo BASE; ?>vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
	<script src="<?php echo BASE; ?>vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
	<script src="<?php echo BASE; ?>vendors/flot.curvedlines/curvedLines.js"></script>
	<!-- DateJS -->
	<script src="<?php echo BASE; ?>vendors/DateJS/build/date.js"></script>
	<!-- JQVMap -->
	<script src="<?php echo BASE; ?>vendors/jqvmap/dist/jquery.vmap.js"></script>
	<script src="<?php echo BASE; ?>vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
	<script src="<?php echo BASE; ?>vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
	<!-- bootstrap-daterangepicker -->
	<script src="<?php echo BASE; ?>vendors/moment/min/moment.min.js"></script>
	<script src="<?php echo BASE; ?>vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

	
	

  	<script src="<?php echo BASE; ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo BASE; ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo BASE; ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo BASE; ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo BASE; ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo BASE; ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo BASE; ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo BASE; ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo BASE; ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo BASE; ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo BASE; ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo BASE; ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo BASE; ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo BASE; ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo BASE; ?>vendors/pdfmake/build/vfs_fonts.js"></script>



    
    <!-- Custom Theme Scripts -->
    <script src="<?php echo BASE;;?>build/js/custom.js"></script>
<script>
	$( "#modal" ).modal( "show" );
	 $("#espaco-modal").html("");
// funcao chamada ao confirmar o modal aberto
	function confirmaSolicitarAvaliacao($projeto) {
		$.post( "<?php echo BASE;?>requisicao/solicita-avaliacao.php", { projeto: $projeto})
  .done(function( data ) {
	$( "#modal" ).modal( "hide" );
	if(data){
		new PNotify({
			title: 'Solicitação realizada com sucesso',
			text: 'Sua solitação para avaliar o projeto foi registrada, aguarde ela ser aprovada!',
			type: 'success',
	  		styling: 'bootstrap3'
		});
	}
	});
	}
	 //ao clicar em solicitar avaliacao
$(".solicitar-avaliacao").on("click", function(){
		// envia o projeto e recebe o modal
		$.post( "<?php echo BASE;?>back-end/modal-confirma-avaliacao.php", { projeto: $(this).attr("id")})
  .done(function( data ) {
			// insere o modal no html e o exibe
    $("#espaco-modal").html(data);
	$( "#modal" ).modal( "show" );
  });

});		
//ao clicar em remover usuario
var $linha=null;
$(".remover-usuario").on("click", function(){
		$linha=$(this).parent().parent();;

		// envia o projeto e recebe o modal
		$.post( "<?php echo BASE;?>back-end/modal-excluir-usuario-global.php", { id_administrador: $(this).attr("id")})
  .done(function( data ) {
			// insere o modal no html e o exibe
    $("#espaco-modal").html(data);
	$( "#modal" ).modal( "show" );

  });

});	
// funcao chamada ao confirmar o modal aberto
	function confirmaRemoverUsuario($administrador) {
		$.post( "<?php echo BASE;?>requisicao/confirma-exclusao-usuario-global.php", { administrador: $administrador, pagina:"<?php echo $pagina?>", categoria:"<?php echo $categoria ?>"})
  .done(function( data ) {
	$( "#modal" ).modal( "hide" );

	if(data==true){
		$linha.remove();
		new PNotify({
			title: 'Usuário removido com sucesso!',
			text: 'O usuário foi removido!',
			type: 'success',
	  		styling: 'bootstrap3'
		});
	}
	else if(data==-1){
new PNotify({
			title: 'Falha!',
			text: 'Você não possui permissão para remover esse usuário!',
			type: 'error',
	  		styling: 'bootstrap3'
		});
	}
	else if (data==false){
		new PNotify({
			title: 'Falha!',
			text: 'Este usuário não pode ser removido!',
			type: 'error',
	  		styling: 'bootstrap3'
		});
	} 
	});
	}
	$("#cadastrar-usuario").on("click",function(){
		var cont=0;
		$("input[required]").each(function(){
          if($(this).val()==0)
          	cont++;
		});
		$("select[required]").each(function(){
          if($(this).val()=="")
          	cont++;
		});
		
		if(cont>0){
			new PNotify({
			title: 'Dados incompletos!',
			text: 'Preencha todos os campos',
			type: 'error',
	  		styling: 'bootstrap3'
		});
		}
		else{
			$("#formulario-padrao").removeAttr("onsubmit");
			$("#formulario-padrao").submit();
		}
	})
</script> 
	
</body>

</html>